package com.everis.tdm.services;

import com.everis.tdm.commons.Util;
import com.everis.tdm.model.giru.Cliente;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.hamcrest.CoreMatchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import static com.everis.tdm.commons.Util.getTemplate;
import static com.everis.tdm.commons.Util.getRegexValue;
import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;


public class CrearCliente {
    private static final Logger LOGGER = LoggerFactory.getLogger(CrearCliente.class);

    static private final String CREAR_CLIENTE_URL = "https://dpiuat.grupoib.local:7200/ibk/srv/MPO/CRM/cliente.crearCliente/v1.0";

    static private final String TEMPLATE_CREAR_CLIENTE = "/request/crearCliente.xml";

    public static Cliente crearClienteBus(Cliente cli) {
        LOGGER.info("Customer Creation started...");

        RestAssured.useRelaxedHTTPSValidation();
    //    cli.setTipoDocumento("DNI");
    //    cli.setNumeroDocumento(String.valueOf(Util.generateDocumentRandom()));
        // TODO: Controlar cantidad de intentos, evitar saturar el servicio
        while (true) {
            String bodyRequest = getTemplate(TEMPLATE_CREAR_CLIENTE).replace("{numDocumento}", "cli.getNumeroDocumento()");
            Response res = given().
                    header("Accept-Encoding", "gzip,deflate").
                    header("Content-Type", "text/xml;charset=UTF-8").
                    header("SOAPAction", "http://interbank.com.pe/service/MPO/CRM/cliente.crearCliente/v1.0/").
                    header("Host", "dpiuat.grupoib.local:7200").
                    body(bodyRequest).
                    when().
                    post(CREAR_CLIENTE_URL);

            if (res.statusCode() == 200) {
                assertThat(getRegexValue(res.getBody().asString(), "<busResponseMessage>(.+?)</busResponseMessage>"), CoreMatchers.equalTo("EJECUCION CON EXITO"));
                assertThat(getRegexValue(res.getBody().asString(), "<codigoUnicoCliente>(.+?)</codigoUnicoCliente>").length(), CoreMatchers.equalTo(10));
                LOGGER.info("Customer created :");
                String customerID = getRegexValue(res.getBody().asString(), "<codigoUnicoCliente>(.+?)</codigoUnicoCliente>");
       //         cli.setCodigoUnico(customerID);
                LOGGER.info(customerID);
                break;
            } else {
                LOGGER.info("Request error:  DEBUG MODE");
            }
            LOGGER.info("CREACION DESACTIVADA");
        }
        return cli;
    }

}
