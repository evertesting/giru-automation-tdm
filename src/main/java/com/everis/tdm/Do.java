package com.everis.tdm;

import com.everis.tdm.config.AppConfig;
import com.everis.tdm.dao.giru.giruDAO;
import com.everis.tdm.model.giru.Cliente;
import com.everis.tdm.model.giru.Reclamo;
import com.everis.tdm.model.giru.Tarjeta;
import com.everis.tdm.services.AltaTarjeta;
import com.everis.tdm.services.CrearCliente;
import com.everis.tdm.web.LoyaltyStep;
import net.thucydides.core.annotations.Steps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import java.util.stream.Collectors;
import java.util.List;

import static com.everis.tdm.security.Decryption.decrypt;
import static com.everis.tdm.security.Encryption.encrypt;


public class Do {

    private static final Logger LOGGER = LoggerFactory.getLogger(Do.class);
    static AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
    static giruDAO dao = context.getBean(giruDAO.class);
    @Steps
    static LoyaltyStep loyaltyStep;

    public static void setNewCliente(String Usuario, String Area, String Contrasena, String Rol) throws Exception {
        Cliente a = new Cliente();
        a.setUsuario(setEncrypt(Usuario));
        a.setArea(Area);
        a.setContrasena(setEncrypt(Contrasena));
        a.setRol(Rol);


        dao.sp_insertCliente(a.getUsuario(), a.getArea(), a.getContrasena(), a.getRol());
        LOGGER.info("Registro de cliente en BD exitoso");

    }

    public static void setNewTarjeta(String numtarjeta, String cvv, String fecha) throws Exception {
        Tarjeta a = new Tarjeta();
        a.setNumtarjeta(setEncrypt(numtarjeta));
        a.setCvv(setEncrypt(cvv));
        a.setFecha(setEncrypt(setEncrypt(fecha)));


        dao.sp_insertTarjeta(a.getNumtarjeta(), a.getCvv(), a.getFecha());
        LOGGER.info("Registro de cliente en BD exitoso");

    }

    public static void setNewUsuario(String Usuario, String Area, String Contrasena, String Rol) throws Exception {
        Cliente a = new Cliente();
        a.setUsuario(setEncrypt(Usuario));
        a.setArea(setEncrypt(Area));
        a.setContrasena(Contrasena);
        a.setRol(Rol);

        dao.sp_insertCliente(a.getUsuario(), a.getArea(), a.getContrasena(), a.getRol());
        LOGGER.info("Registro de cliente en BD exitoso");

    }

    public static void updateMillas(String codigoUnico, int millasTarjeta) {
        Cliente a = new Cliente();
        //a.setCodigoUnico(codigoUnico);
        //a.setMillasTarjeta(millasTarjeta);
        //dao.sp_updateMillas(a.getCodigoUnico(), a.getMillasTarjeta());
//        LOGGER.info("Millas actualizadas en BD para el cliente {}", a.getCodigoUnico());

    }

    public static void updateRegistroBenni(String codigoUnico) {
        Cliente a = new Cliente();
   //     a.setCodigoUnico(codigoUnico);
    //    dao.sp_updateRegistroBenni(a.getCodigoUnico());
        LOGGER.info("Registro actualizado");
    }

    public static List<Cliente> getListClientes() {
        return dao.sp_getClientes();
    }
    public static List<Tarjeta> getListTarjetas() {
        return dao.sp_getTarjetaPorEstado("congelado");
    }
        public static Tarjeta getTarjetaPorEstado(String vestado)
        {
            Tarjeta tar;
            List<Tarjeta> list = dao.sp_getTarjetaPorEstado(vestado).stream().collect(Collectors.toList());
            tar = list.stream().findAny().orElse(list.stream().findAny().orElse(null));
            LOGGER.info("Cliente obtenido de TDM " + tar);
            return tar;

        }
    public static Cliente recargarMillas(Cliente cli) throws InterruptedException {
      //  LOGGER.info("Inicia la recarga de Millas para el cliente {}", cli.getCodigoUnico());

        //loyaltyStep.openLoyalty();
        //loyaltyStep.doLogin();
       // loyaltyStep.verifyLogin();
       // loyaltyStep.doMillas(cli);
        //updateMillas(cli.getCodigoUnico(), cli.getMillasTarjeta());
        return cli;
    }

    public static Cliente crearCliente(Cliente cli) {
        return CrearCliente.crearClienteBus(cli);
    }

    public static Cliente crearTarjeta(Cliente cli) {
        return AltaTarjeta.altaTarjeta(cli);
    }

    public static Cliente usuarioRegistroTDM(String Usuario, String Area, String Contrasena, String Rol) throws Exception {
        Cliente cli = new Cliente();
        cli.setArea(Area);
        cli.setUsuario(Usuario);
        cli.setContrasena(Contrasena);
        cli.setRol(Rol);
            setNewCliente(Usuario,Area,Contrasena,Rol);
            return cli;
    }
    public static Tarjeta usuarioTarjetaTDM(String numtarjeta, String cvv, String fecha) throws Exception {
        Tarjeta tar = new Tarjeta();
        tar.setNumtarjeta(numtarjeta);
        tar.setCvv(cvv);
        tar.setNumtarjeta(fecha);
        setNewTarjeta(numtarjeta,cvv,fecha);
        return tar;
    }
    public static Cliente clienteRegistroTDM() throws Exception {
        Cliente cli = new Cliente();
     /*   Optional<Cliente> optional = getListClientes().stream().filter(x -> !x.isRegistroBenni()).findFirst();
        if (optional.isPresent()) {
            LOGGER.info("Cliente existe en BD");
            cli = optional.get();
            if (cli.getNumeroTarjeta().isEmpty())
                crearTarjeta(cli);
            // TODO: update db with new credit card
            LOGGER.info("EL CLIENTE OBTENIDO DE LA BD ES :");
            LOGGER.info(String.valueOf(cli));
        } else {
            LOGGER.info("No existen clientes en BD");
            crearCliente(cli);
            crearTarjeta(cli);
            setNewCliente(cli.getTipoDocumento(), cli.getNumeroDocumento(), cli.getCodigoUnico(), cli.getTipoTarjeta(), cli.getNumeroTarjeta()
                    , cli.getFechaCaducidadTarjeta(), 0, "0", "231400Aa.", "");
        }
        return cli;
*/



        return null;
    }

    public static Cliente crearClienteTarjeta() throws Exception {
        Cliente cli = new Cliente();
        crearCliente(cli);
        crearTarjeta(cli);
    //    setNewCliente(cli.getTipoDocumento(), cli.getNumeroDocumento(), cli.getCodigoUnico(), cli.getTipoTarjeta(), cli.getNumeroTarjeta()
      //          , cli.getFechaCaducidadTarjeta(), 0, "0", "", "");

        return cli;

    }

    public static Cliente getUsuarioPorRolTdm(String rol) throws InterruptedException {

       /* cli = list.stream().filter(x -> x.getMillasTarjeta() >= 10000).findAny().orElse(list.stream().findAny().orElse(null));
        if (cli == null) {
            LOGGER.info(" No existen clientes registrados, se procede a crear Cliente TDM:");
            //TODO: Crear Cliente TDM + Millas
            crearCliente(cli);
            crearTarjeta(cli);
            recargarMillas(cli);
        } else if (cli.getMillasTarjeta() < 10000) {
            LOGGER.info("El cliente tiene pocas millas");
            //recargarMillas(cli);
        }

*/
        Cliente cli;
        List<Cliente> list = dao.sp_getUsuarioPorRol(rol).stream().collect(Collectors.toList());
         cli = list.stream().findAny().orElse(list.stream().findAny().orElse(null));
        LOGGER.info("Cliente obtenido de TDM " + cli);
        return cli;

    }

    public static Cliente clienteMillasTdm() throws InterruptedException {
        Cliente cli;
      /*  List<Cliente> list = Do.getListClientes().stream().filter(Cliente::isRegistroBenni).collect(Collectors.toList());
        cli = list.stream().filter(x -> x.getMillasTarjeta() >= 10000).findAny().orElse(list.stream().findAny().orElse(null));
        if (cli == null) {
            LOGGER.info(" No existen clientes registrados, se procede a crear Cliente TDM:");
            //TODO: Crear Cliente TDM + Millas
            crearCliente(cli);
            crearTarjeta(cli);
            recargarMillas(cli);
        } else if (cli.getMillasTarjeta() < 10000) {
            LOGGER.info("El cliente tiene pocas millas");
            //recargarMillas(cli);
        }
        LOGGER.info("Cliente obtenido de TDM " + cli);

        return cli;*/
        return null;
    }


    public static String setEncrypt(String value) throws Exception {
        return encrypt(value);
    }

    public static String getDecrypt(String value) throws Exception {
        return decrypt(value);
    }

    //--------------------------------------------------------------- Renzo

    //Registrar un reclamo en la BD
    public static void setNewReclamo(String tipologia, String medioRespuesta) throws Exception {
        Reclamo a = new Reclamo();
        a.setTipologia(setEncrypt(tipologia));
        a.setMedioRespuesta(medioRespuesta);

        dao.sp_insertReclamo(a.getTipologia(), a.getMedioRespuesta());
        LOGGER.info("Registro de Reclamo en BD exitoso");

    }

    //Traer la lista de todos los reclamos en la BD
    public static List<Reclamo> getListReclamos() {
        return dao.sp_getReclamos();
    }

    public static Reclamo usuarioReclamoTDM(String tipologia, String medioRespuesta) throws Exception {
        Reclamo recl = new Reclamo();
        recl.setTipologia(tipologia);
        recl.setMedioRespuesta(medioRespuesta);
        setNewReclamo(tipologia,medioRespuesta);
        return recl;
    }

}
