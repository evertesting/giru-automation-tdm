package com.everis.tdm.model.giru;

import lombok.SneakyThrows;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.everis.tdm.security.Decryption.decrypt;

public class TarjetaMapper implements RowMapper<Tarjeta> {
    @SneakyThrows
    public Tarjeta mapRow(ResultSet rs, int arg1) throws SQLException {
        Tarjeta data = new Tarjeta();
        data.setNumtarjeta(decrypt(rs.getString("numtarjeta")));
        data.setCvv(decrypt(rs.getString("cvv")));
        data.setFecha(rs.getString("fecha"));

        return data;
    }
}
