package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@Setter
public class Usuario {

    private String usuario;
    private String contrasena;
    private String rol;
    private String area;


}
