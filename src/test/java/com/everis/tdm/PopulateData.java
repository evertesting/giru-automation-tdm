package com.everis.tdm;

import com.everis.tdm.model.giru.Cliente;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PopulateData {

   // @Test
    public void regsitroClientes() throws Exception {
      /*  Do.setNewCliente("DNI", "10217784", "0060529726", "",
                (""), (""), 0, "",
                (""), "JesusOvalle/prueba.benni.everis");

        Do.setNewCliente("DNI", "10217786", "0060529727", "",
                (""), (""), 0, "",
                (""), "JesusOvalle/prueba.benni.everis");*/
    }

   // @Test
    public void getCientes() {
        Do.getListClientes().forEach(System.out::println);
    }


   // @Test
    public void updateMillas() {
        Do.updateMillas("0060102593", 0);
    }


   // @Test
    public void beeniProductos() {
     /*   Cliente cli = Do.getListClientes().stream().findFirst().get();

        System.out.println("ESTE CLIENTE ES :" + cli.getNumeroDocumento());
        System.out.println("SU TARJETA ES   :" + cli.getNumeroTarjeta());
        System.out.println("SUS MILLAS SON  :" + cli.getMillasTarjeta());

        //TODO CANJE DE PRODUCTOS EN BEENI
        // CANJEAR UN TOTAL DE PRODUCTOS POR 3000 MILLAS
        System.out.println("CLIENTE REALIZA EL CANJE DE PRODCUTOS CON 7000 MILLAS");

        Do.updateMillas(cli.getCodigoUnico(), cli.getMillasTarjeta() - 7000);
        System.out.println("EL CLIENTE AHORA TIENE " + Do.getListClientes().stream().findFirst().get().getMillasTarjeta()
                + " MILLAS EN TOTAL");*/
    }

   // @Test
    public void crearClienteConTarjetaGruadarDB() throws Exception {
        for (int i = 0; i < 10; i++)
            Do.crearClienteTarjeta();
    }

    @Test
    public void tdmCrearCliente() throws Exception {
        Do.usuarioRegistroTDM("xt9285", "Area", "Callao2121++", "Supervisor");
    }
    @Test
    public void tdmCrearTarjeta() throws Exception {
        Do.usuarioTarjetaTDM("411093******9687", "475", "07/2024");
    }
   // @Test
    public void filtroClientesNoRegistrados() {
    //    Stream<Cliente> optional = Do.getListClientes().stream().filter(x -> !x.isRegistroBenni());
     //   optional.forEach(System.out::println);
    }


   // @Test
    public void getClientesTdmParaCanjes() {
    /*    List<Cliente> list = Do.getListClientes().stream().filter(Cliente::isRegistroBenni).collect(Collectors.toList());
        Cliente opt = list.stream().filter(x -> x.getMillasTarjeta() >= 10000).findAny().orElse(list.stream().findAny().orElse(null));
        if (opt == null)
            System.out.println(" No existen clientes registrados con millas sufiente, se procede a realizar la recarga:");
            //TODO: Crear Cliente TDM + Millas
        else if (opt.getMillasTarjeta() < 10000)
            System.out.println("El cliente tiene pocas millas:");
        //TODO: AUMENTAR MILLAS POR LOYALTY
        System.out.println(opt);*/

    }

   // @Test
    public void clienteMillasTDM() throws InterruptedException {
        System.out.println(Do.clienteMillasTdm());
    }

    //@Test
    public void encriptarPassword() throws Exception {
        System.out.println(Do.setEncrypt("231400Aa."));


    }

    //--------------------------------------------------------------- Renzo
    @Test
    public void getListReclamos() {
        Do.getListReclamos().forEach(System.out::println);
    }

    @Test
    public void setNewReclamo() throws Exception {
        //System.out.println(Do.setNewReclamo("Cobros Indebidos", "2"));
        Do.setNewReclamo("Cobros Indebidos", "5");
        //Do.setNewReclamo("Cobros Indebidos", "2").forEach(System.out::println);
    }

    //creat reclamo
    @Test
    public void tdmCrearReclamo() throws Exception {
        Do.usuarioReclamoTDM("Cobros Indebidos", "6");
    }

}